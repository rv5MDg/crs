### This was used to study larger l-primes ###

include("../src/includes.jl")

########## Key exchange ##########
##### Part 1: Walk forward #####
### Key
l = 39323
warmup_key = Dict(l => 1)
key = Dict(l => 1)
print("Key:\n")
dict_show(key)

### Walk
print("time for ", l)
walk(base_E, warmup_key)
endpoint =  @time walk(base_E, key)

### Key
l = 24979
warmup_key = Dict(l => 1)
key = Dict(l => 1)
print("Key:\n")
dict_show(key)

### Walk
print("time for ", l)
walk(base_E, warmup_key)
endpoint =  @time walk(base_E, key)


### Key
l = 22501
warmup_key = Dict(l => 1)
key = Dict(l => 1)
print("Key:\n")
dict_show(key)

### Walk
print("time for ", l)
walk(base_E, warmup_key)
endpoint =  @time walk(base_E, key)


### Key
l = 3929
warmup_key = Dict(l => 1)
key = Dict(l => 1)
print("Key:\n")
dict_show(key)

### Walk
print("time for ", l)
walk(base_E, warmup_key)
endpoint =  @time walk(base_E, key)



###### Part 2: Walk backward #####
#print("\n")
#### Key
#warmup_key = Dict(7 => -1)
#key = Dict(7 => -1)
#print("Key:\n")
#dict_show(key)
#
##### Walk
#walk(endpoint, warmup_key)
#res =  @time walk(endpoint, key)
#
#print("jinvariant Efinal:\n")
#print(j_invariant(res))
#print("\n")
#
#@assert j_invariant(res) == j_invariant(base_E)
