##### Differential equations solving #####

##### LIN SOLVE #####
F, _ = Nemo.FiniteField(101, 1, "u")
R, y = PowerSeriesRing(F, 10, "y")
a, b, c, alpha = R(1) + y , R(3) + y^2, R(1)+ y + y^5 + y^9 + Nemo.O(y^10), R(4)
n = 7
sol = linSolve(a, b, c, alpha, n)
print(sol)
#####

##### NON LIN SOLVE #####
F, _ = Nemo.FiniteField(101, 1, "u")
R, y = PowerSeriesRing(base_F, 15, "y")
Rt, t = PolynomialRing(R, "t")

alpha, beta = R(1), R(2)
G = (R(4) + y)*Rt(1) + (y^2 + y)*t + y^5 * t^5
n = 11
sol = nonLinSolve(G, alpha, beta, n)
#####
