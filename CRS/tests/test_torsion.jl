##### Torsion ####
println("Base curve:")
println(base_E)
println(j_invariant(base_E))
print("\n")

E_SW = SW_model(base_E)
println("Curve in SW form:")
println(E_SW)
println(j_invariant(E_SW))
print("\n")

println("Trace:")
println(base_t)
print("\n")

key = Dict(3 => 1)
Eprime = walk(base_E, key)
println("Target:")
println(Eprime)
print("\n")

Eprime_SW = SW_model(Eprime)
println("Target in SW form:")
println(Eprime_SW)
#################
