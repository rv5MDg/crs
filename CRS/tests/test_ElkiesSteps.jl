### None of this currently work properly ###

## Elkies
include("Elkies/c_libs.jl")
include("Elkies/bipolys.jl")
include("Elkies/elkies.jl")
include("Elkies/solveEq.jl")

include("EllipticCurves/kernel.jl")

@enum Direction::Int8 left=-1 right=1

struct ElkiesPrimeData
    max_steps::UInt16
    ev_left::Integer
    ev_right::Integer
    mod_pol_cls::AbstractAlgebra.Generic.MPoly{Nemo.fq}
    mod_pol_atk::AbstractAlgebra.Generic.MPoly{Nemo.fq}
end

ElkiesPrimes = Dict(
    #### Elkies primes
    	11  => ElkiesPrimeData(100, 10, 9, load_Classical(11, CRS.base_F), load_Atkin(11, CRS.base_F)),
	23  => ElkiesPrimeData(100, 13, 7, load_Classical(23, CRS.base_F), load_Atkin(23, CRS.base_F)),
	5  => ElkiesPrimeData(100, 1, 4, load_Classical(5, CRS.base_F), load_Atkin(5, CRS.base_F))
    )


### Test 101
#E1 = ShortWeierstrass(base_F(1), base_F(1))
#E2 = ShortWeierstrass(base_F(75), base_F(16))
#
##
### Test ElkiesWalk
#l = 11
#k = 1
#j2 = ElkiesWalk(E1, l, right, k)
#println(j2)

### Compare
#println(j_invariant(E2))

## Test 512
#l=11
#E1_ = base_E
#key = Dict(l => 1)
#E2_ = walk(E1_, key)
#E1 = SW_model(E1_)
#E2 = SW_model(E2_)
#kernel_poly(E1, E2, l)

## Test ElkiesWalk
l = 5
k = 1
E1 = SW_model(base_E)

println("going left")
j2 = ElkiesWalk(E1, l, left, k)
println(j2)
print("\n\n")

println("going right")
j2 = ElkiesWalk(E1, l, right, k)
println(j2)

### Compare
#key = Dict(l => -k)
#E2_ = walk(base_E, key)
#println(j_invariant(E2_))

# Test 101
#E1 = ShortWeierstrass(base_F(1), base_F(1))
#E2 = ShortWeierstrass(base_F(75), base_F(16))
#
## Test ElkiesWalk
#l = 11
#kp = kernel_poly(E1, E2, l)
#println(kp)
#println(j_invariant(E2))
