include("../src/CRS.jl")

########## Key exchange ##########

### Private keys
print("Private key for Alice\n")
#private_key_alice = CRS.keyGeneration()
private_key_alice = Dict(7 => -2, 3 => 1, 5 => 1)
CRS.dict_show(private_key_alice)
print("Private key for Bob\n")
#private_key_bob = CRS.keyGeneration()
private_key_bob = Dict(7 => -1, 3 => 1, 5 => -1)
CRS.dict_show(private_key_bob)
print("\n\n")

### Public keys
print("Public key for Alice\n")
public_key_alice =  CRS.walk(CRS.base_E, private_key_alice)
print(public_key_alice)
print("\nPublic key for Bob\n")
public_key_bob =  CRS.walk(CRS.base_E, private_key_bob)
print(public_key_bob)
print("\n\n")

### Secret reconstruction
print("Alice finds the following secret\n")
secret_alice =  CRS.walk(public_key_bob, private_key_alice)
print(secret_alice)
print("\nBob finds the following secret\n")
secret_bob =  CRS.walk(public_key_alice, private_key_bob)
print(secret_bob)
print("\n\n")

### Check integrity of the secrets
j_alice = CRS.j_invariant(secret_alice)
j_bob = CRS.j_invariant(secret_bob)
println("These secrets respectively correspond to the following j-invariants:")
print("j_alice\n")
print(j_alice)
print("\nj_bob\n")
print(j_bob)
