# CRS Documentation

```@meta
CurrentModule = CRS
DocTestSetup = quote
    using CRS
end
```

```@contents
```

## Unstable
```@example
julia> ]
pkg> add Hone#Unstable
```

## Index
```@index
```

```@autodocs
Modules = [CRS]
```
