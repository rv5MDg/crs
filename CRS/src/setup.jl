######################################################################
# setup.jl: Generate global environment for CRS
######################################################################

export setup, F_ext, u_ext, f_ext

######################################################################
# Generation of extensions and cardinals
######################################################################

"""
Dictionary where keys are degrees 1 <= d <= max_r and values are
cardinalities of E(F_p^d)
"""
function create_cardExtensions(max_r::Integer, t::Nemo. fmpz, q::Nemo.fmpz)
	cardExtensions = Dict(1 => card_over_extension(1))
	if max_r == 1
		return cardExtensions
	else
		sn_ = 2
		sn = t
		for r in 2:max_r
			tmp = sn
			sn = t*sn - q*sn_
			sn_ = tmp
			cardExtensions[r] = q^r + 1 - sn
		end
	end
	return cardExtensions
end


"""
Cardinality of the d-th extension of the base field
"""
function card_ext(r::Integer)
	return cardExtensions[r]
end

"""
Dictionary where keys are degrees 1 <= r <= max_r and values are
tuples of the form (K, u_r, f_r) where F_r, u_r = FiniteField(base_p, r, "u")
and f_r = embed(F_1, F_r)
"""
function create_fieldExtensions(max_r::Integer, p::Nemo.fmpz)
	F_1, u_1 = Nemo.FiniteField(p, 1, "u_1")
	f_1 = Nemo.embed(F_1, F_1)
	fieldExtensions = Dict(1 => (F_1, u_1, f_1))

	for r in 2:max_r
		F_r, u_r = Nemo.FiniteField(p, r, "u_r")
		f_r = Nemo.embed(F_1, F_r)
		fieldExtensions[r] = (F_r, u_r, f_r)
	end
	return fieldExtensions
end

"""
d-th extension of the base field
"""
function F_ext(d::Integer)
	return fieldExtensions[d][1]
end

"""
Generator of the d-th extension of the base field
"""
function u_ext(d::Integer)
	return fieldExtensions[d][2]
end

"""
Embedding of the base field into its d-th extension
"""
function f_ext(d::Integer)
	return fieldExtensions[d][3]
end

######################################################################
# Global parameters
######################################################################

### get the config file for global parameters
conf = ConfParse(PATH_CONFIG)
parse_conf!(conf)

## setup global constants and algebraic structures
const ZZ = Nemo.ZZ
const base_t = ZZ(ConfParser.retrieve(conf, "E_param", "base_t"))
const base_p = ZZ(ConfParser.retrieve(conf, "base_param", "base_p"))
const base_q = ZZ(ConfParser.retrieve(conf, "base_param", "base_q"))

const l_lBound = parse( Int64, ConfParser.retrieve(conf, "l_param", "l_lBound"))
const l_hBound = parse( Int64, ConfParser.retrieve(conf, "l_param", "l_hBound"))
const r_lBound = parse( Int64, ConfParser.retrieve(conf, "l_param", "r_lBound"))
const r_hBound = parse( Int64, ConfParser.retrieve(conf, "l_param", "r_hBound"))

const max_r = parse( Int64, ConfParser.retrieve(conf, "base_param", "max_r"))
const cardExtensions = create_cardExtensions(r_hBound, base_t, base_q)
const fieldExtensions = create_fieldExtensions(max_r, base_q)

const base_F = F_ext(1)
const base_card = card_ext(1)

const base_A = base_F(ZZ(ConfParser.retrieve(conf, "E_param", "base_A")))
const base_B = base_F(ZZ(ConfParser.retrieve(conf, "E_param", "base_B")))
const base_E = Montgomery(base_A, base_B)
#const base_E = ShortWeierstrass(base_F(1), base_F(1))

const base_frob = frobeniusPolynomial(base_E, base_card)

## load or create primes
#bounds = JSON.parsefile(PATH_BOUNDS)
#lPrimes = generate_primes(l_lBound, l_hBound, r_lBound, r_hBound, bounds)
#export_primes(lPrimes)
lPrimes = import_primes()
