module CRS
	using JSON
	using Nemo
	using ConfParser
	using AbstractTrees
	using AbstractAlgebra

	PATH_CONFIG = "../confs/config.ini"
	PATH_PRIMES = "../confs/primes.json"
	PATH_BOUNDS = "../optimization/files/optimized.json"

	import Base: show, isvalid, ==, rand, *, +, -, digits
	import ClassPolynomials

	## global structures and types
	include("structures.jl")

	## elliptic curves
	include("./EllipticCurves/structures.jl")
	include("./EllipticCurves/points.jl")
	include("./EllipticCurves/montgomery.jl")
	include("./EllipticCurves/montgomerypoints.jl")
	include("./EllipticCurves/tateNormal.jl")
	include("./EllipticCurves/isogenies.jl")
	include("./EllipticCurves/weierstrass.jl")
	include("./EllipticCurves/weierstrasspoints.jl")
	include("./EllipticCurves/finiteFields.jl")
	include("./EllipticCurves/models.jl")

	## additional utils
	include("./utils/tools.jl")
	include("utils/binarytree_core.jl")
	include("utils/remainderTree.jl")
	include("utils/prettyPrint.jl")

	## primes and keys
	include("primes.jl")
	include("keys.jl")

	## walk
	include("walk.jl")

	## initialize global parameters
	include("setup.jl")

	## Elkies
	#include("./EllipticCurves/bmss.jl")
	#include("EllipticCurves/kernel.jl")
	#include("Elkies/c_libs.jl")
	#include("Elkies/bipolys.jl")
	#include("Elkies/elkies.jl")
	#include("Elkies/solveEq.jl")


end
