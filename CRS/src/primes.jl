######################################################################
# primes.jl: Generation and storage of l-primes
######################################################################

export import_primes, export_primes, generate_primes

"""
Load l-primes from a json file into a dictionary.
"""
function import_primes()
	lPrimes = Dict()
	tmp = JSON.parsefile(PATH_PRIMES)
	for (l, data) in tmp
		## parse l
		l = parse(Int64, l)

		## gather parsed data
		type_ = data["type"]
		lBound = data["lBound"]
		hBound = data["hBound"]
		minOrder = data["minOrder"]
		maxOrder = data["maxOrder"]
		backward = data["backward"]
		r = data["r"]

		## add prime to dictionary
		if type_ == "Radical"
			lPrimes[l] = RadicalPrime(l, lBound, hBound, minOrder, maxOrder, r, backward)
		elseif type_ == "Velu"
			lPrimes[l] = VeluPrime(l, lBound, hBound, minOrder, maxOrder, r, backward)
		else
			lPrimes[l] = ElkiesPrime(l, lBound, hBound, minOrder, maxOrder, r, backward)
		end
	end
	return lPrimes
end

"""
Export l-primes from a dictionary into a json file.
"""
function export_primes(lPrimes)
	data = Dict()
	for (l, lPrime) in lPrimes

		## get type of prime l
		if typeof(lPrime) == RadicalPrime
			type_ = "Radical"
		elseif typeof(lPrime) == VeluPrime
			type_ = "Velu"
		elseif typeof(lPrime) == ElkiesPrime
			type_ = "Elkies"
		else
			throw(ArgumentError("Malformed prime"))
		end

		## add l to the data
		data[l] = Dict( "type" => type_,
				"lBound" => lPrime.lBound,
				"hBound" => lPrime.hBound,
				"minOrder" => lPrime.minOrder,
				"maxOrder" => lPrime.maxOrder,
				"backward" => lPrime.backward,
				"r" => lPrime.r)
	end

	## write to file
	open(PATH_PRIMES, "w") do f
	    JSON.print(f, data)
	end
	return true
end

"""
Create l-primes dictionary.
"""
function generate_primes( l_lBound = 3,
			l_hBound = 1500,
			r_lBound = 1,
			r_hBound = 9,
			M_bounds = Dict())

	## placeholder for all usable primes
	lPrimes = Dict{Integer, Prime}()

	## classify primes
	for l in [p for p in l_lBound:l_hBound if Nemo.isprime(p)]

		## roots of the frobenius mod l
		Fl, _ = Nemo.FiniteField(l, 1, "u")
		FlX, _ = Nemo.PolynomialRing(Fl, "X")
		l_frob = FlX(base_frob)
		rts = Nemo.roots(l_frob)

		## only keep primes of type Elkies
		if length(rts) < 2 continue end

		## compute orders
		rts = (rts[1], rts[2])
		o1, o2 = order(rts[1]), order(rts[2])
		minOrder, maxOrder = min(o1, o2), max(o1, o2)

		## switch to pass non-compliant primes
		usable = true


		## Check if l is of type Elkies, Velu or Radical
		if (o1 != o2) & (minOrder < max_r)
			## here l is Velu or Radical

			## check in which extension the l-torsion points exist
			l_r = -1
			for r in r_lBound:r_hBound
				## check if the order of E[F_p^r] is divisible by l
				## in such a case, an element of order l exists
				if card_ext(r) % l == 0
					l_r = r
					break
				end

			end
			## if no torsion below the r_hBound threshold exists
			## discard the prime
			if l_r == -1 usable = false  end

			## check if backward motion is possible in the twist
			## TODO: an hybride with Elkies might be possible
			backward = div(maxOrder, 2) > max_r ? false : true

			## set the bounds
			if length(M_bounds) == 0
				lBound = 0
				hBound = 1
			else
				lBound = -M_bounds[string(l)]["backward"]
				hBound = M_bounds[string(l)]["forward"]
			end

			## set the type
			if (l == 3 || l == 5) & usable
				lPrime = RadicalPrime(	l,
							lBound,
							hBound,
				    			minOrder,
							maxOrder,
							l_r,
							backward)
			elseif usable
				lPrime = VeluPrime(	l,
							lBound,
							hBound,
				    			minOrder,
							maxOrder,
							l_r,
							backward)
			end
		else
			usable = false

		#elseif minOrder < max_r
		#		lPrime = VeluPrime(	l,
		#					lBound,
		#					hBound,
		#		    			minOrder,
		#					maxOrder,
		#					l_r,
		#					true)

		end

		## create lPrime from gathered data
		if usable
			lPrimes[l] = lPrime
		end
	end
	return lPrimes
end
