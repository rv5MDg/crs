export  eval_fromtree, remainderTree

function treeEval(g, roots_array, RX, X)
	k = length(roots_array)
	n = Nemo.degree(g)
	if n <= k
		return eval_fromroots(g, roots_array, RX, X)
	else
		# Naive multiplication
		M = RX(1)
		for x in roots_array
			M = M*(X-x)
		end
		g = g % M
		return eval_fromroots(g, roots_array, RX, X)
	end
end

# Eval using tree
function eval_fromtree(g, B)
	R = Nemo.base_ring(parent(g))

	if !isdefined(B, :data)
		return R(1)
	elseif !isdefined(B, :left)
		return Nemo.lead(g % (B.data))
	else
		left = eval_fromtree(g, B.left)
		right = eval_fromtree(g, B.right)
		return left * right
	end
end



# direct Eval
function eval_direct(g, roots_array, RX, X)
	#computes prod(g(x)) for x in roots_array
	R = Nemo.base_ring(RX)
	n = length(roots_array)

	if n == 0
		return BinaryNode(R(1))

	elseif n == 1
		# Bad lifting?
		return BinaryNode(Nemo.lead(g % (X-roots_array[1])))

	else
		m = div(n, 2)
		left = eval_fromroots(g, roots_array[1:m], RX, X)
		right = eval_fromroots(g, roots_array[m+1:n], RX, X)

		P = left.data * right.data
		B = BinaryNode(P)

		left.parent = B
		right.parent = B

		B.left = left
		B.right = right

		return B
	end
end

function remainderTree(roots_array, RX, X)
	n = length(roots_array)
	if n == 0
		return BinaryNode(RX(1))
	elseif n == 1
		return BinaryNode(X-roots_array[1])
	else
		m = div(n, 2)
		left = remainderTree(roots_array[1:m], RX, X)
		right = remainderTree(roots_array[m+1:n], RX, X)

		P = left.data * right.data
		B = BinaryNode(P)

		left.parent = B
		right.parent = B

		B.left = left
		B.right = right

		return B
	end
end
