export any_root, convert, valuation

function any_root(f::Nemo.PolyElem{T}) where T<:Nemo.FinFieldElem
	r = Nemo.roots(f)
	for root in r
		return (true, root)
	end
	return (false, Nemo.zero(Nemo.base_ring(f)))
end

function convert(x::Nemo.FinFieldElem, K::Nemo.FinField)
	K1 = parent(x)
	p1 = Nemo.characteristic(K1)
	p = Nemo.characteristic(K)
	(p1 == p) || throw(ArgumentError("Fields must have the same characteristic"))
	y = deepcopy(x)
	y.parent = K
	return y
end

function valuation(p::Integer, x::Integer)
	pow = 0
	while (x % p^pow) == 0
		pow += 1
	end
	return pow - 1
end

function valuation(p::Integer, x::Nemo.fmpz)
	pow = 0
	while (x % p^pow) == 0
		pow += 1
	end
	return pow - 1
end

"""
Polynomial exponentiation for big exponents
"""
function res_exp(P::AbstractAlgebra.Generic.Res{Nemo.fq_poly}, k::BigInt)
	K = parent(P)
	if k == 0
		return K(1)
	elseif k == 1
		return P
	else
		if k % 2 == 0
			Q = res_exp(P, div(k, 2))
			return Q^2
		else
			Q = res_exp(P, div(k-1, 2))
			return P*Q^2
		end
	end
end

function res_exp(P::Nemo.fq_poly, k::Integer)
	K = parent(P)
	if k == 0
		return K(1)
	elseif k == 1
		return P
	else
		if k % 2 == 0
			Q = res_exp(P, div(k, 2))
			return Q^2
		else
			Q = res_exp(P, div(k-1, 2))
			return P*Q^2
		end
	end
end
function res_exp(P::AbstractAlgebra.Generic.Res{AbstractAlgebra.Generic.Poly{AbstractAlgebra.Generic.Res{Nemo.fq_poly}}}, k::Integer)
	K = parent(P)
	if k == 0
		return K(1)
	elseif k == 1
		return P
	else
		if k % 2 == 0
			Q = res_exp(P, div(k, 2))
			return Q^2
		else
			Q = res_exp(P, div(k-1, 2))
			return P*Q^2
		end
	end
end
