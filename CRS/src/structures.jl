######################################################################
# structures.jl: Types and struct for CRS
######################################################################

export StepType, Elkies, Velu, Rad
export Direction
export VeluPrime, ElkiesPrime, RadicalPrime

######################################################################
# Structures for walking in isogeny graphs
######################################################################

"""
Abstract type for l-primes
"""
abstract type Prime end

@enum Direction::Int8 left=-1 right=1

"""
Type for primes using the iso_elkies algorithm
"""
struct ElkiesPrime <: Prime
	l::Integer
	lBound::Integer
	hBound::Integer
	minOrder::Integer
	maxOrder::Integer
	r::Integer
	backward::Bool
	## Should add the modular polynomial here for BMSS or kerpoly
end

"""
Type for primes using the iso_sqrtVelu algorithm
"""
struct VeluPrime <: Prime
	l::Integer
	lBound::Integer
	hBound::Integer
	minOrder::Integer
	maxOrder::Integer
	r::Integer
	backward::Bool
end

"""
Type for primes using the iso_rad algorithm
"""
struct RadicalPrime <: Prime
	l::Integer
	lBound::Integer
	hBound::Integer
	minOrder::Integer
	maxOrder::Integer
	r::Integer
	backward::Bool
end
