######################################################################
# walk.jl: Walks and steps in isogeny graphs
######################################################################

export walk

######################################################################
# SqrtVelu steps and walk
######################################################################

"""
Taking a step from E in the direction dir using the SqrtVelu algorithm.
Parameter dir is equal to 1 for forward walking and -1 for walking
on the quadratic twist.
"""
function step_velu(E, l, dir = 1)
	## get the working degree
	r = lPrimes[l].r

	## using a quadratic twist if walking backward
	if dir == -1
		r *= 2
	end

	## compute the step
	P = torsionXZ(E, l, r)
	Eext = P.curve
	Eres = iso_sqrtVelu(Eext, l, P)
	A_ = convert(Eres, base_F)
	return  Montgomery(A_, base_F(1))
end

"""
Taking k steps from E in the l-isogeny graph.
"""
function walk_velu(E, l, k)
	Eprime = E
	dir = k >= 0 ? 1 : -1
	for _ in 1:abs(k)
		Eprime = step_velu(Eprime, l, dir)
	end
	return Eprime
end

######################################################################
# Walk in isogeny graph
######################################################################

"""
Using a key to walk in the isogeny graph.
"""
function walk(E, key)
	Eprime = E
	for (l, k) in key
		if k == 0
			continue
		elseif l == 3 || l == 5
			### Radical steps
			Eprime = iso_radical(Eprime, l, k)
		else
			### Velu steps
			Eprime = walk_velu(Eprime, l, k)
		end
	end
	return Eprime
end
