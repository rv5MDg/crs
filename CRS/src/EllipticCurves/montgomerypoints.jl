######################################################################
# montgomerypoints.jl: xz-points for Montgomery curves
######################################################################

export XZPoint, xadd, xdouble, xinfinity, fixedtorsion, isfixedtorsion, XZzero

######################################################################
# Types, definitions and simple operations
######################################################################

"""
Concrete type for xz-points on Montgomery curves.
"""
mutable struct XZPoint{T<:Nemo.RingElem} <: ProjectivePoint{T}
    X::T
    Z::T
    curve::Montgomery{T}
end

"""
Base ring of the point.
"""
base_ring(P::XZPoint) = Nemo.parent(P.X)

"""
Base ring of the curve.
"""
base_curve(P::XZPoint) = P.curve

"""
Description of an xz-point.
"""
show(io::IO, P::XZPoint) = print(io, "($(P.X):$(P.Z))")

"""
Create an xz-point from a projective point with 3 coordinates.
"""
function XZPoint(P::EllipticPoint{T}) where T<:Nemo.RingElem
    return P.Z == 0 ? xinfinity(P.curve) : XZPoint(P.X, P.Z, P.curve)
end

"""
Normalized xz-point from any xz-point.
Set P to (0, 0) if P is infinity, otherwise set P to (X/Z, 1).
"""
function normalize(P::XZPoint{T}) where T
    R = Nemo.parent(P.X)
    if isinfinity(P)
        return XZPoint(Nemo.one(R),Nemo.zero(R),P.curve)
    else
        return XZPoint(P.X // P.Z,Nemo.one(R),P.curve)
    end
end

"""
In-place normalization.
"""
function normalize!(P::XZPoint{T}) where T<:Nemo.FieldElem
    K = Nemo.parent(P.X)
    if isinfinity(P)
        P.X = Nemo.one(K)
    else
        P.X = P.X // P.Z
        P.Z = Nemo.one(K)
    end
    return
end

"""
Check whether two xz-points are given by the exact same coordinates.
"""
function samefields(P::XZPoint, Q::XZPoint)
	return (P.curve == Q.curve) & (P.X == Q.X) & (P.Z == Q.Z)
end

"""
Check whether two xz-points are equal in the base ring.
"""
==(P::XZPoint, Q::XZPoint) = P.X * Q.Z - P.Z * Q.X == 0

"""
Check whether an xz-point is the point at infinity.
"""
isinfinity(P::XZPoint) = Nemo.iszero(P.Z)

"""
Decide whether an xz-point is the fixed 2-torsion point of the Montgomery model (at the origin).
"""
isfixedtorsion(P::XZPoint) = Nemo.iszero(P.X)

"""
The xz-point at infinity on a Montgomery curve.
"""
function xinfinity(E::Montgomery)
    K = Nemo.parent(E.A)
    return XZPoint(Nemo.one(K), Nemo.zero(K), E)
end

"""
The zero xz-point of the group of points.
"""
function XZzero(E::Montgomery)
	return xinfinity(E)
end

"""
The fixed xz- 2-torsion point on a Montgomery curve.
"""
function fixedtorsion(E::Montgomery)
    R = Nemo.parent(E.A)
    return XZPoint(Nemo.zero(R), Nemo.one(R), E)
end

"""
Validity of an xz-point.
"""
function isvalid(P::XZPoint)
    return !(P.X == P.Z == 0) && issquare( P.curve.B * ( P.X^2 + P.curve.A*P.X*P.Z + P.Z^2 ) * P.X * P.Z )[1]
end

######################################################################
# Montgomery arithmetic
######################################################################
"""
Double any xz-point using the least possible field operations.
"""
function xdouble(P::XZPoint{T}) where T
    E = P.curve
    R = base_ring(E)

    v1 = P.X + P.Z
    v1 = v1^2
    v2 = P.X - P.Z
    v2 = v2^2

    X2 = v1 * v2

    v1 = v1 - v2
    v3 = ((E.A + 2) // 4) * v1
    v3 = v3 + v2

    Z2 = v1 * v3

    return XZPoint(X2, Z2, E)
end

"""
Differential addition on xz-points using the least possible field operations.
This function assumes the difference is not (0:0) or (0:1).
"""
function xadd(P::XZPoint{T}, Q::XZPoint{T}, Minus::XZPoint{T}) where T
    v0 = P.X + P.Z
    v1 = Q.X - Q.Z
    v1 = v1 * v0

    v0 = P.X - P.Z
    v2 = Q.X + Q.Z
    v2 = v2 * v0

    v3 = v1 + v2
    v3 = v3^2
    v4 = v1 - v2
    v4 = v4^2

    Xplus = Minus.Z * v3
    Zplus = Minus.X * v4

    return XZPoint(Xplus, Zplus, P.curve)
end

"""
Montgomery ladder to compute scalar multiplications of generic xz-points, using the least possible field operations.
"""
function xladder(k::Nemo.fmpz, P::XZPoint)
    x0, x1 = P, xdouble(P)
    for b in Iterators.drop(Iterators.reverse(digits(Int8, BigInt(k), base=2)), 1)
        if (b == 0)
        	x0, x1 = xdouble(x0), xadd(x0, x1, P)
        else
        	x0, x1 = xadd(x0, x1, P), xdouble(x1)
        end
    end
    return x0
end

"""
Large scalar multiplication with xz-points
"""
function *(k::Nemo.fmpz, P::XZPoint)
    E = P.curve
    if k == 0
	return xinfinity(E)
    elseif k<0
	return (-k) * P
    else
	if isinfinity(P)
	    return xinfinity(E)
	elseif isfixedtorsion(P)
	    if k % 2 == 0
		return xinfinity(E)
	    else
		return fixedtorsion(E)
	    end
	else
	    return xladder(k, P)
	end
    end
end


"""
Scalar multiplication with xz-points
"""

*(k::Integer, P::XZPoint) = *(Nemo.ZZ(k), P)

"""
Efficient computation of points lambda*P where lambda=k+s for k=low to k=high-1.
"""
function xadd_step(P::XZPoint, low, high, s)
	if low > high
		throw(ArgumentError("Wrong bound parameters"))
	elseif s == 0
		throw(ArgumentError("Step parameter is null"))
	elseif isinfinity(P)
		throw(ArgumentError("P is infinity"))
	end

	Q0 = (low-s)*P
	Q1 = low*P
	L = []
	sP = s*P
	for k in low:s:high
		push!(L, Q1)
		if isinfinity(Q0)
			tmp = Q1
			Q1 = 2*Q1
			Q0 = tmp
		else
			tmp = Q1
			Q1 = xadd(Q1, sP, Q0)
			Q0 = tmp
		end
	end
	return L
end
