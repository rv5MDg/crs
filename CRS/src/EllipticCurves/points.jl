######################################################################
# points.jl: Points on elliptic curves
######################################################################

export EllipticPoint, Point
export coordinates, base_curve, isinfinity, normalize!, normalized, samefields

mutable struct EllipticPoint{T<:Nemo.RingElem} <: ProjectivePoint{T}
	X::T
	Y::T
	Z::T
	curve::EllipticCurve{T}
end

coordinates(P::EllipticPoint) = (P.X, P.Y, P.Z)
base_curve(P::EllipticPoint) = P.curve

function Point(x::T, y::T, curve::EllipticCurve{T}) where T
	R = base_ring(x)
	return EllipticPoint(x, y, R(1), curve)
end
function Point(X::T, Y::T, Z::T, curve::EllipticCurve{T}) where T
	return EllipticPoint(X, Y, Z, curve)
end

#
# Methods
#

base_ring(P::EllipticPoint) = parent(P.X)

function is_valid(P::EllipticPoint)
	Eq = projective_equation(P.curve)
	x, y, z = coordinates(P)
	return Eq(x, y, z) == 0 && (x, y, z) != (0, 0, 0)
end

show(io::IO, P::EllipticPoint) = print("Point (", P.X, ":", P.Y, ":", P.Z, ") on ", P.curve)

function isinfinity(P::EllipticPoint)
	R = base_ring(P)
	return P.Z == Nemo.zero(R)
end

function normalized(P::EllipticPoint)
    R = base_ring(P)
    if isinfinity(P)
        return EllipticPoint(Nemo.zero(R),one(R),Nemo.zero(R),P.curve)
    else
        return EllipticPoint(P.X // P.Z,P.Y // P.Z,one(R),P.curve)
    end
end
function normalize!(P::EllipticPoint{T}) where T<:Nemo.FieldElem
    R = base_ring(P)
    if isinfinity(P)
        P.X = zero(R)
        P.Y = one(R)
        P.Z = zero(R)
    else
        P.X = P.X // P.Z
        P.Y = P.Y // P.Z
        P.Z = one(R)
    end
    return
end

==(P::EllipticPoint, Q::EllipticPoint) = samefields(normalized(P), normalized(Q))
samefields(P::EllipticPoint, Q::EllipticPoint) = (P.curve == Q.curve) & (P.X == Q.X) & (P.Y == Q.Y) & (P.Z == Q.Z)
