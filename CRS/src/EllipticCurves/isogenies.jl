######################################################################
# isogenies.jl: isogenie representation and computation
######################################################################

export iso_sqrtVelu, iso_radical, iso_velu

######################################################################
# SqrtVelu algorithm
######################################################################
"""
Extract x coordinate from array of Montgomery points.
"""
lambda(Q) = !Nemo.iszero(Q.Z) ? Q.X*inv(Q.Z) : 0

"""
Prepares the three sets of x-coordinates to use SqrtVelu
"""
function KPS(P::XZPoint{T}, l::Integer) where T
	## 3 is a special case because of the infinity point
	if l == 3
		XZPoint_K = [P, 3*P]
		K_ = broadcast(lambda, XZPoint_K)
		return [], [], K_
	end

	b1::Integer = floor(sqrt(l-1)/2)
	b2::Integer = (b1 == 0) ? 1 : floor((l-1)/(4*b1))

	S = xadd_step(P, 1, l-1, 2)

	XZPoint_I =  2*b1*S[1:b2]
	I_ = broadcast(lambda, XZPoint_I)

	XZPoint_J = S[1:b1]
	J_ = broadcast(lambda, XZPoint_J)

	XZPoint_K = S[2*b1*b2 + 1:length(S)]
	K_ = broadcast(lambda, XZPoint_K)

	return I_, J_, K_
end

"""
Computes an l-isogeny from E using the three sets I, J, K defined with KPS.
"""
function xISOG(E::Montgomery{T}, l::Integer, I, J, K) where T
	R = base_ring(E)
	RW, W = Nemo.PolynomialRing(R, "W")
	RXZ, (X, Z) = Nemo.PolynomialRing(R, ["X", "Z"])

	F0 = Z^2 - 2*X*Z + X^2
	F1 = -2*(X*Z^2 + (X^2 + 2*E.A*X + 1)*Z + X)
	F2 = X^2*Z^2 - 2*X*Z + 1

	E0_J = RXZ(1)
	E1_J = RXZ(1)
	for x_J in J
		S = F0(X, x_J) + F2(X, x_J)
		E0_J = E0_J*( S+F1(X, x_J) )
		E1_J = E1_J*( S-F1(X, x_J) )
	end

	E0_J_W = Nemo.to_univariate(RW, E0_J)
	E1_J_W = Nemo.to_univariate(RW, E1_J)

	if typeof(E0_J_W) == Nemo.fq && typeof(E1_J_W) == Nemo.fq
		R0 = E0_J_W^(length(I))
		R1 = E1_J_W^(length(I))
	else
		B =  remainderTree(I, RW, W)
		R0 =  eval_fromtree(E0_J_W, B)
		R1 =  eval_fromtree(E1_J_W, B)
	end

	M0 = R(1)
	M1 = R(1)
	for x_K in K
		 M0= M0*(R(1)-x_K)
		 M1= M1*(R(-1)-x_K)
	end

	d = ( (E.A-2)*inv(E.A+2) )^l * ( (M0*R0)*inv(M1*R1))^8

	return 2*(1+d)*inv(1-d)
end

"""
Computes an l-isogeny from E with kernel <P> using SqrtVelu.
"""
function iso_sqrtVelu(E::EllipticCurve, l::Integer, P::XZPoint)
	I, J, K = KPS(P, l)
	return xISOG(E, l, I, J, K)
end

######################################################################
# Radical isogenies algorithm
######################################################################

"""
Computes an l^k-isogeny from E with starting kernel <P>.
If k is negative, the isogeny composition is performed over
the quadratic twists.
"""
function iso_radical(E::Montgomery, l::Integer, k::Integer = 1)
	if k == 0 return E end

	R = F_ext(1)			# F_p

	## looking for an l-torsion point
	if k < 0
		## F_p^2 is necessary to work in the quadratic twist of E:
		## to put E in Tate Normal Form, the y-coordinate of P is used.
		## Arithmetic involving x will stays in F_p.

		P = torsionXZ(E, l, 2)			# P is defined over F_p^2 with P.x in F_p
		E = base_extend(P.curve, 2)		# E is now defined over F_p^2
		f = Nemo.embed(R, base_ring(E))
		P = XZPoint(f(P.X), f(P.Z), E)		# P is now defined over F_p^2
		k = -k
	else
		P = torsionXZ(E, l, 1)
	end

	## putting E in Tate-normal form
	E_tn = TN_model(E, P, l)


	## performing the k steps to an l^k-isogeny
	if l == 3
		a1, a3 = base_ring(E)(1) - E_tn.c, -E_tn.b

		## 3rd root extraction
		## https://eprint.iacr.org/2013/024.pdf
		r1 = div(base_p^2-1, 3)
		r2 = div(r1-1,3)
		for i in 1:k
			alpha = (-a3)^(-r2)
			tmp = a1
			a1 = -6*alpha + a1
			a3 = 3*tmp*alpha^2 - tmp^2*alpha + 9*a3
		end
		Eprime = TateNormal(-a3, base_ring(E)(1)-a1, l)
		return M_model(Eprime)
	elseif l == 5
		b = E_tn.b

		## 5th root extraction
		e = div(base_p+1, 5)
		for _ in 1:k
			alpha = Nemo.sqrt(b^e) #fifth root of b
			if alpha^5 != b
				alpha = -alpha
			end
			frac_top = alpha^4 + 3*alpha^3 + 4*alpha^2 + 2*alpha + 1
			frac_bot = inv(alpha^4 - 2*alpha^3 + 4*alpha^2 - 3*alpha + 1)
			b = alpha * frac_top * frac_bot
		end
		Eprime = TateNormal(base_F(Nemo.coeff(b,0)), base_F(Nemo.coeff(b,0)), l)
		return M_model(Eprime)
	end

end

######################################################################
# Original Velu algorithm
######################################################################

"""
Computes an l-isogeny from E with <P> as kernel using the Velu formula.
"""
function iso_velu(E::EllipticCurve{T}, l::Integer, P::XZPoint) where T
	R = base_ring(E)
	RX, X = Nemo.PolynomialRing(R, "X")

	h_S = RX(1)

	diff = Nemo.ZZ(-1)*P
	D = 2*P
	Q = P
	for k in 0:div(l-3, 2)
		x = Q.X*inv(Q.Z)
		h_S = h_S*(X-x)

		tmp = Q
		Q = xadd(Q, D, diff)
		diff = tmp
	end

	d = (( (E.A-2)*inv(E.A+2) )^l) * ( h_S(R(1))*inv(h_S(R(-1))) )^8

	return 2*(1+d)*inv(1-d)
end
