######################################################################
# kernel.jl: Kernel polynomial for l-isogeny between curves.
######################################################################
export kernel_poly

include("../Elkies/solveEq.jl")
include("../Elkies/convertSeries.jl")

"""
	http://www.numdam.org/item/JTNB_1995__7_1_219_0.pdf
	Sum of x-coordinates in the kernel.
"""
function sum_p1(E1::EllipticCurve{T}, E2::EllipticCurve{T}, l::Integer) where T<:FieldElem

	## base ring for elliptic curves
	R = base_ring(E1)

	## j-invariants
	j1 = j_invariant(E1)
	j2 = j_invariant(E2)

	## modular polynomial
	lPrime = ElkiesPrimes[l]
	phi = lPrime.mod_pol_cls

	## multivariate ring for modular polynomial
	FXY = parent(phi)
	X, Y = Nemo.gens(FXY)

	## partial derivatives of phi
	phi_X = Nemo.derivative(phi, 1)
	phi_Y = Nemo.derivative(phi, 2)
	phi_XX = Nemo.derivative(phi_X, 1)
	phi_YY = Nemo.derivative(phi_Y, 2)
	phi_XY = Nemo.derivative(phi_X, 2)

	## partial derivatives evaluated at (j1, j2)
	phi_X_j = phi_X(j1, j2)
	phi_Y_j = phi_Y(j1, j2)
	phi_XX_j = phi_XX(j1, j2)
	phi_YY_j = phi_YY(j1, j2)
	phi_XY_j = phi_XY(j1, j2)

	### recover j2'
	j2_ = -R(18)*inv(R(l)) * E1.b*inv(E1.a) * phi_X_j*inv(phi_Y_j) * j1
	### recover j1'
	j1_ = -R(l)*j2_*phi_Y_j * inv(phi_X_j)

	### recover target curve
	AA = -inv(R(48)) * j2_^2*inv(j2*(j2-R(1728)))
	BB = -inv(R(864)) * j2_^3*inv( j2^2*(j2-R(1728)))

	## j1''/j1' - l * j2''/j2'
	diff1 = -(j1_^2*phi_XX_j + R(2*l)*j1_*j2_*phi_XY_j + R(l)^2*j2_^2*phi_YY_j) * inv(j1_*phi_X_j)

	## j1'/j1 - l *  j2'/j2
	diff2 = j1_*inv(j1) - R(l)*j2_*inv(j2)

	##
	diff3 = j1_*inv(j1-R(1728)) - R(l)*j2_*inv(j2-R(1728))

	## E_2(p) - l(E_2(p^l))
	diffE2 = R(6)*diff1 - R(3)*diff3 - R(4)*diff2

	## sum
	p1 = inv(R(12))*R(l)*diffE2

	return p1, AA, BB
end


"""
	http://www.lix.polytechnique.fr/~morain/jcomp.pdf
	Kernel polynomial of the l-isogeny between E1 and E2.
"""
function kernel_poly(E1::EllipticCurve{T}, E2::EllipticCurve{T}, l::Integer) where T<:FieldElem

	R = base_ring(E1)
	Rx, x = PowerSeriesRing(R, 2*l+2, "x", model=:capped_absolute)
	Rxt, t = PolynomialRing(Rx, "t")

	## sum of x non zero coordinates divided by 2 (degree of kernel_poly)
	p1, AA, BB = sum_p1(E1, E2, l)
	p1 = inv(R(2))*p1

	## compute S mod x^(2l+3)
	C = inv(E1.b*x^6 + E1.a*x^4 + Rx(1))
	G = C * (Rx(1) + E2.a*t^4 + E2.b*t^6)

	S = nonLinSolve(G, Rx(0), Rx(1), 2*l+3)

	## recover T
	T_ = T_from_S(S)
	set_prec!(T_, l+1)

	## find h-coefficients
	U = inv(T_^2)
	RX, X = PolynomialRing(R, "X")
	N_D_poly = truncate(reverse(convert(RX, U)), l)
	h_list = reverse([x for x in coefficients(N_D_poly)][1:l-1])[1:div(l-1,2)-1]

	## find p-coefficients
	p_list = [p1]
	for i in 1:div(l-1,2)-1
		if i == 1
			p_next = inv(R(4*i+2)) * (h_list[i]-E1.a*(R(l)-1))
		elseif i == 2
			p_next = inv(R(4*i+2)) * (h_list[i] - R(2)*E1.b*(R(l)-1) - R(6)*E1.a*p1)
		else
			p_next = inv(R(4*i+2)) * (h_list[i] - (R(4*i-2))*E1.a*p_list[i-1] - (R(4*i-4))*E1.b*p_list[i-2])
		end
		push!(p_list, p_next)
	end

	g_ = Rx(0)
	set_prec!(g_, div(l-1, 2))
	for i in 1:length(p_list)
		g_ += p_list[i]*x^(i-1)
	end

	g = reverse(convert(RX, Nemo.exp(-Nemo.integral(g_))))
	g *= inv(lead(g))
	return g
end
