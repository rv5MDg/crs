include("../Elkies/solveEq.jl")
include("../Elkies/convertSeries.jl")
include("../Elkies/reconstruction.jl")

######################################################################
# bmss.jl: kernel polynomial via differential equations, NOT WORKING RIGHT NOW
######################################################################
function unsafe_kernelpoly_(E1::EllipticCurve{T}, E2::EllipticCurve{T}, deg::Integer) where T<:FieldElem
	K = base_ring(E1)
	#E1_SW = SW_model(E1)
	#E2_SW = SW_model(E2)
	E1_SW = E1
	E2_SW = E2
	#####################################

	#We need precision 8*deg + 5
	R, x = PowerSeriesRing(K, 8*deg +5, "x", model=:capped_absolute)
	Rt, t = PolynomialRing(R, "t")

	### SOLVE EQUATION FOR S ############
	C = inv(E1_SW.b*x^6 + E1_SW.a*x^4 + R(1))
	G = C * (R(1) + E2_SW.a*t^4 + E2_SW.b*t^6)

	alpha = R(0)
	beta = R(1)
	S = nonLinSolve(G, alpha, beta, 8*deg + 5)

	## Sanity check for S
	#println(coeff(S, 5))
	#println(inv(base_F(10)) * (E2_SW.a-E1_SW.a))
	#print("\n\n")
	#println(coeff(S, 7))
	#println(inv(base_F(14)) * (E2_SW.b-E1_SW.b))
	#lhs = (E1_SW.b*x^6 + E1_SW.a*x^4 + 1)*derivative(S)^2
	#rhs = (1 + E2_SW.a*S^4 + E2_SW.b*S^6)
	#println(lhs == rhs)
	#println(lhs)
	#print("\n\n")
	#println(rhs)

	#####################################

	### GET T  ##########################
	T_ = T_from_S(S)
	set_prec!(T_, 4*deg+2)

	## Sanity check for S = xT(x^2)
	#println(S)
	#print("\n")
	#println(x*composeXSquare(T_))
	#return
	#####################################

	### GET U  ##########################
	U = Nemo.inv(T_^2)

	#####################################


	### recover the fraction for U ######
	RX, X = PolynomialRing(K, "X")
	U_poly = convert(RX, U)
	approx = X^(4*deg+2)
	num, den = wang2(approx, U_poly)

	#U_poly = truncate(convert(RX, U), 2*deg)
	#num, den = berlekamp(U_poly)
	#println(den)
	#print("\n\n\n")

	## sanity check U = num/den
	#@assert (den*U_poly%approx == num)

	#####################################

	### recover N and D #################
    	N = X*reverse(num)
    	D = reverse(den)

    	# If the points of abscissa 0 are in the kernel, correct the degree of D
    	gap = degree(N) - degree(D) - 1
    	(gap > 0) && (D = shift_left(D, gap))

    	#Making denominator monic
    	lambda = inv(Nemo.lead(D))
    	D = lambda * D
    	N = lambda * N
	#####################################

	return N, D
end
