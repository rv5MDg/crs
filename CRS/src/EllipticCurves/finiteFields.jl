######################################################################
# finfields.jl: Elliptic curves over finite fields
######################################################################

export frobeniusTrace, cardinality, frobeniusPolynomial,torsionPoint,card_over_extension, randXZ,  torsionXZ
export base_extend


######################################################################
# Trace, cardinality and Frobenius polynomial
######################################################################


function frobeniusTrace(E::EllipticCurve{T}) where T<:Nemo.FinFieldElem
	if base_p == 101 return -18 end
	return -147189550172528104900422131912266898599387555512924231762107728432541952979290
	#Call PARI
	throw(NotImplementedError("Link to PARI to compute cardinalities of elliptic curves over finite fields"))
end

function cardinality(E::EllipticCurve{T}) where T<:Nemo.FinFieldElem
	t = frobeniusTrace(E)
	R = base_ring(E)
	p = Nemo.characteristic(R)
	return p + 1 - t
end

function card_over_extension(r::Integer) where T
	t, q = base_t, base_q

	if r == 1
		return q + 1 - t
	else
		sn_ = 2
		sn = t
		for n in 2:r
			tmp = sn
			sn = t*sn - q*sn_
			sn_ = tmp
		end
	end
	return q^r + 1 - sn
end

### Using an elliptic curve
function card_over_extension(E::EllipticCurve{T}, r::Integer) where T
	t = frobeniusTrace(E)
	R = base_ring(E)
	q = Nemo.order(R)

	if r == 1
		return q + 1 - t
	else
		sn_ = 2
		sn = t
		for n in 2:r
			tmp = sn
			sn = t*sn - q*sn_
			sn_ = tmp
		end
	end
	return q^r + 1 - sn
end

function frobeniusPolynomial(E::EllipticCurve{T}, Card) where T<:Nemo.FinFieldElem
	q = Nemo.order(base_ring(E))
	t = q + 1 - Card
	A, X = Nemo.PolynomialRing(Nemo.ZZ, "X")
	return X^2 - t * X + q
end

function order(x::Nemo.FinFieldElem)
	K = parent(x)
	if x == Nemo.zero(K)
		throw(ArgumentError("Argument must be non-zero"))
	else
		k = 1
		z = x
		while z != Nemo.one(K)
			z *= x
			k += 1
		end
		return k
	end
end


######################################################################
# Random points on elliptic curves
######################################################################

function rand(E::EllipticCurve{T}) where T<:Nemo.FinFieldElem
	R = base_ring(E)
	A, Y = Nemo.PolynomialRing(R, "Y")
	x = Nemo.rand(R)
	poly = equation(E, x, Y)
	bool, y = any_root(poly)
	while bool == false
		x = Nemo.rand(R)
		poly = equation(E, x, Y)
		(bool, y) = any_root(poly)
	end
	return Point(x, y, Nemo.one(R), E)
end

function randXZ(E::Montgomery{T}) where T<:Nemo.FinFieldElem
	P = rand(E)
	return XZPoint(P)
end


######################################################################
# Torsion points
######################################################################

function torsionPoint(E::EllipticCurve{T}, l, Card) where T<:Nemo.FinFieldElem
	cofactor = Nemo.div(Card, l)
	@assert cofactor * l == Card
	P = rand(E)
	Q = cofactor * P
	while isinfinity(Q)
		P = rand(E)
		Q = cofactor * P
	end
	isinfinity(l * Q) || throw(ArgumentError("Given curve has no such rational torsion points"))
	return Q
end

"""
	torsionXZ(E::EllipticCurve{T}, l, r = 1) where T<: Nemo.FinFieldElem

Returns a q^r-rational XZ-point P of specified torsion
Should r be divisible by 2, returns P with P.X in F_q^r//2
"""

function torsionXZ(E::EllipticCurve{T}, l, r = 1) where T<: Nemo.FinFieldElem
	## parameters to check for l-torsion in F_p^r
	Cr = cardExtensions[r]
	v = valuation(l, Cr)
	#v = 1
	@assert v >= 1
	cofactor = div(Cr, l^v)
	@assert cofactor * l^v == Cr

	## parameters to reduce to P.X in F_p^r//2 if possible
	is_reducible = r % 2 == 0
	r_prime = is_reducible ? div(r, 2) : r

	## base change
	L = is_reducible ? F_ext(r_prime) : F_ext(r)
	Eext = base_extend(E, r_prime)

	## initializing parameters
	if is_reducible x = L(1) end
	Q = XZPoint(L(1), L(0), E)

	## main loop
	while isinfinity(Q)
		if is_reducible
			# looking for (x, y) coordinates with x in F_q^r//2 and y in F_q^r
			is_square = true
			while is_square
				# pick an x in F_p^r//2
				x = Nemo.rand(L)
				s = x*(x^2 + Eext.A*x + L(1))

				# check if y is a square root in L, discard if true
				is_square = s^( div(base_p^r_prime-1, 2))  == L(1)
			end
			P = XZPoint(x, L(1), Eext)
		else
			P = randXZ(Eext)
		end
		Q = cofactor * P

	end

	## creates an l-torsion point out of Q (of l^v-torsion)
	c = 1 # safety
	while (c <= v) & (!isinfinity(l*Q))
		Q = l*Q
		c += 1
	end

	## sanity check
	isinfinity(l * Q) || throw(ArgumentError("Given curve has no rational l-torsion points"))
	return Q
end


######################################################################
# Base extensions
######################################################################


function base_change(E::AbstractWeierstrass{T}, K::Nemo.FinField) where T<:Nemo.FinFieldElem
	CurveType = curvetype(E)
	K1 = base_ring(E)
	p1 = characteristic(K1)
	p = characteristic(K)
	a1, a2, a3, a4, a6 = a_invariants(E)
	((degree(K1) == 1) & (p1 == p)) || throw(ArgumentError("Invalid field extension"))
	return CurveType(convert(a1, K), convert(a2, K), convert(a3, K), convert(a4, K), convert(a6, K))
end

###
#	Embeddings only work from degree 1 to degree d (for big characteristic)
# 	So here R is supposed to be of degree 1
###
function base_extend(E::Montgomery{T}, r::Integer) where T<:Nemo.FinFieldElem
	if Nemo.degree(base_ring(E)) == r return E end
	base_ring(E) == base_F || throw(ArgumentError("Invalid field extension"))
	f_r = f_ext(r)
	return Montgomery(f_r(E.A), f_r(E.B))
end

function base_extend(E::TateNormal{T}, r::Integer) where T<:Nemo.FinFieldElem
	if Nemo.degree(base_ring(E)) == r return E end
	base_ring(E) == base_F || throw(ArgumentError("Invalid field extension"))
	f_r = f_ext(r)
	return TateNormal(f_r(E.b), f_r(E.c), E.N)
end

function base_extend(E::ShortWeierstrass{T}, r::Integer) where T<:Nemo.FinFieldElem
	if Nemo.degree(base_ring(E)) == r return E end
	base_ring(E) == base_F || throw(ArgumentError("Invalid field extension"))
	f_r = f_ext(r)
	return Montgomery(f_r(E.a), f_r(E.b))
end

######################################################################
# Existence of a Montgomery model
######################################################################

#function has_montgomery(E::ShortWeierstrass{T}) where T<:Nemo.FinFieldElem
#	K = base_ring(E)
#	A, X = PolynomialRing(K, "X")
#	_, _, _, a, b = a_invariants(E)
#	poly = X^3 + a * X + b
#	r = roots(poly)
#	n = length(r)
#	for i = 1:n
#		alpha, _ = r[i]
#		(test, beta) = issquare(3*alpha^2 + E.a)
#		test && return (true, Montgomery(3 * alpha // beta, 1 // beta))
#	end
#	return (false, Montgomery(K(1), K(1)))
#end

# This is unbelievably stupid
function lift(n::Nemo.fq)
	q = Nemo.order(parent(n))
	m = parse(Nemo.fmpz, string(n))
	return (m >= 0) ? m : q-m
end
