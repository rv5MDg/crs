######################################################################
# normalTate.jl: Normal-Tate model
######################################################################

export TateNormal

######################################################################
# Basic methods
######################################################################

"""
Concrete type for Normal-Tate.
"""
struct TateNormal{T<:Nemo.RingElem} <: EllipticCurve{T}
    	b::T
    	c::T
	N::Integer
end

"""
Base ring of Normal-Tate curve.
"""
function base_ring(E::TateNormal)
	return Nemo.parent(E.b)
end

"""
Description of a Tate normal curve.
"""
function show(io::IO, E::TateNormal{T}) where T
    #print(io, "Elliptic Curve  $(E.b) y² + (1-$E.c) - $E.b = x³ - $(E.b) x² over ")
	print("b: ", E.b)
	print("\n")
	print("c: ", E.c)
	print("\n")
	print("N: ", E.N)
	print("\n")
    #show(io, base_ring(E))
end

"""
The j-invariant of a Tate normal Curve.
"""
function j_invariant(E::TateNormal{T}) where T<:Nemo.FieldElem
	R = base_ring(E)
	one, zero = R(1), R(0)
	return j_invariant(Weierstrass(one-E.c, -E.b, -E.b, zero, zero))
end

"""
Projectivization of the curve equation.
"""
function projective_equation(E::TateNormal)
	K = base_ring(E)
	A, (X, Y, Z) = PolynomialRing(K, ["X", "Y", "Z"])
	return Z*Y^2 + (R(1) - E.c)*Z*X*Y - E.b*Y*z^2 - (X^3 - E.b * X^2*Z)
end

"""
Equation in standard form.
"""
function equation(E::TateNormal)
	R = base_ring(E)
	A, (X, Y) = PolynomialRing(K, ["X", "Y"])
	return Y^2 + (R(1) - E.c)*XY - E.b*Y - (X^3 - E.b * X^2)
end

"""
Equation in standard form with specificaly named variables.
The coefficients of the elliptic curve must be embedable in the parent ring of the variables.
"""
function equation(E::TateNormal, X, Y)
	R = base_ring(E)
	return Y^2 + (R(1) - E.c)*XY - E.b*Y - (X^3 - E.b * X^2)
end
