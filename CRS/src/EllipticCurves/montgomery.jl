######################################################################
# montgomery.jl: Montgomery models
######################################################################

export Montgomery

######################################################################
# Basic methods
######################################################################

"""
Concrete type for Montgomery curves.
"""
struct Montgomery{T<:Nemo.RingElem} <: EllipticCurve{T}
    A::T
    B::T
end

"""
Base ring of Mongomery curve.
"""
function base_ring(E::Montgomery)
	return Nemo.parent(E.A)
end

"""
Description of a Montgomery curve.
"""
function show(io::IO, E::Montgomery{T}) where T
    print(io, "Elliptic Curve  $(E.B) y² = x³ + $(E.A) x² + x  over ")
    show(io, base_ring(E))
end

"""
The j-invariant of a Montgomery Curve.
"""
function j_invariant(E::Montgomery{T}) where T<:Nemo.FieldElem
	# https://eprint.iacr.org/2017/212.pdf
	R = base_ring(E)
	zero = Nemo.zero(R)
	return R(256) * (E.A^2  - R(3))^3 * inv(E.A^2 - 4)
end

"""
Projectivization of the curve equation.
"""
function projective_equation(E::Montgomery)
	K = base_ring(E)
	A, (X, Y, Z) = PolynomialRing(K, ["X", "Y", "Z"])
	return E.B * Z * Y^2 - X^3 - E.A * X^2 * Z - X * Z^2
end

"""
Equation in standard form.
"""
function equation(E::Montgomery)
	K = base_ring(E)
	A, (X, Y) = PolynomialRing(K, ["X", "Y"])
	return E.B * Y^2 - X^3 - E.A * X^2 - X
end

"""
Equation in standard form with specificaly named variables.
The coefficients of the elliptic curve must be embedable in the parent ring of the variables.
"""
function equation(E::Montgomery, X, Y)
	R = base_ring(E)
	return R(E.B) * Y^2 - X^3 - R(E.A) * X^2 - X
end

"""
Validity of an elliptic curve.
Element B(A^2-4) must be non-zero.
"""
function isvalid(E::Montgomery)
	return (E.B != 0) & (E.A != 0) & (E.A != 2) & (E.A != -2)
end
