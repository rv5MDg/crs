######################################################################
# representation.jl: Elliptic curve representation and transformation
######################################################################

export TN_model, M_model, W_model, SW_model

######################################################################
# Montgomery <-> Weierstrass
######################################################################

"""
Transforms an elliptic curve in Montgomery form into one in Weierstrass form.
"""
function W_model(E::Montgomery)
	R = base_ring(E)
	return Weierstrass(R(0), E.A*inv(E.B), R(0), inv(E.B)^2, R(0))
end

"""
Transforms an elliptic curve in Montgomery form into one in short Weierstrass form.
"""
function SW_model(E::Montgomery)
	R = base_ring(E)
	a = (R(3)-E.A^2) * inv(3*E.B^2)
	b = (R(2)*E.A^3-R(9)*E.A) * inv(R(27)*E.B^3)
	return ShortWeierstrass(a, b)
end

"""
Transforms an elliptic curve in Montgomery form into one in short Weierstrass form.
This is not always possible.
"""
function M_model(E::ShortWeierstrass)
	R = base_ring(E)
	RX, X = PolynomialRing(R, "X")
	a, b = E.a, E.b
	P = X^3 + a*X + b
	success, alpha = any_root(P)
	if  !success throw(ArgumentError("Curve does not admit a Montgomery model")) end
	s = R(0)
	try
		s = inv( Nemo.sqrt(R(3)*alpha^2+a) )
	catch e
		throw(ArgumentError("Curve does not admit a Montgomery model"))
	end
	A = R(3)*alpha*s
	B = s

	## try to simplify B
	try
		Nemo.sqrt(s)
		B = R(1)
	catch e
		pass
	end
	return Montgomery(A, B)
end

function M_model(E::Weierstrass)
	a1, a2, a3, a4, a6 = a_invariants(E)
	if a1 == 0 && a3 == 0 && a2 == 0
		return M_model(ShortWeierstrass(a4, a6))
	end
	throw(ArgumentError("Not implemented yet for general Weierstrass curves."))
end

######################################################################
# Montgomery <-> TateNormal
######################################################################

"""
Transforms an elliptic curve in Montgomery form into one in normal Tate form.
The point P must be of order 3 or 5
"""
function TN_model(E::Montgomery, P::XZPoint, N::Integer)
	#Returns E in (almost-)normal Tate form (see Radical Isogeny p.13)

	#### Getting P = (x, y) from Montgomery point
	x = P.X * inv(P.Z)
	R = base_ring(E)
	RW, W = Nemo.PolynomialRing(R, "W")
	RXY, (X, Y) = Nemo.PolynomialRing(R, ["X", "Y"])
	f = equation(E, X, Y)
	fW = Nemo.to_univariate(RW, f(x, Y))
	success, y = any_root(fW)
	if !success
		throw(ArgumentError("Could not find y-coordinate for P"))
	end

	#### Tate normal form for different values of N
	## In the degenerate case where N = 3:
	## 	we set 1-c=a1 and -b=a3
	##	but a2=0 !
	if N == 3
		a1 = (3*x^2 + 2*E.A*x + 1) * inv(y)
		a3 = 2*y
		return TateNormal(-a3, R(1)-a1, 3)
	elseif N == 5
		A2 = 3*x + E.A
		A4 = 3*x^2 + 2*E.A*x + 1
		A3 = 2*y

		gamma = A2 - (A4*inv(A3))^2
		b = -gamma^3 * inv(A3)^2
		return TateNormal(b, b, 5)
	end
end

"""
Transforms an elliptic curve in Normal Tate-form into one in Montgomery form.
"""
function M_model(E::TateNormal)
	## global parameters
	R = base_ring(E)
	p = Nemo.characteristic(R)
	b, c, N = E.b, E.c, E.N

	#### degenerate case N = 3
	if N == 3
		a1, a3 = 1-c, -b
		## we need to work in F_p^2 to extract a root of B4 for beta
		FY, Y = Nemo.PolynomialRing(R, "Y")

		# complete the Y square to kill A1
		A2 = (a1)^2*inv(R(4))
		A4 = a1*a3*inv(R(2))
		A6 = (a3)^2*inv(R(4))

		## complete the X cube to kill A6
		Q = A6 + A4*Y + A2*Y^2 + Y^3

		## Is there a way to find the correct root directly?
		for x in Nemo.roots(Q)
			B2 = R(3)*x + A2
			B4 = R(3)*x^2 + R(2)*A2*x + A4

			## Normalize X and Y to set B4 = 1
			root_test = B4^(div(p-1, 2)) == R(1)
			if !root_test
				## First Check that a root of B4 is defined over F_p
				continue
			else
				## Then Look for a square root of B4
				beta = Nemo.sqrt(B4)

				# Finally, get the A constant
				A = B2 * inv(beta)
				return Montgomery(base_F(Nemo.coeff(A, 0)), base_F(1))
			end
		end
		throw(ArgumentError("Could not find Montgomery model for E"))

	elseif N >= 5
		RX, X = Nemo.PolynomialRing(R, "X")

		## complete the Y square to kill Y
		B2 = ( (R(1)-c)^2 * inv(R(4)) - b )
		B4 = b* (c - R(1)) * inv(R(2))
		B6 = b^2 * inv(R(4))

		## complete the X cube to kill B6
		## can we predict which root(s) will produce a square root for beta?
		P = B6 + B4*X + B2*X^2 + X^3
		for x in Nemo.roots(P)
			try
				A2 = R(3)*x + B2
				A4 = R(3)*x^2 + R(2)*B2*x + B4

				# Scale to set A4 = 1
				beta = Nemo.sqrt(A4)

				# Get A constant
				A = A2 * inv(beta)
				return Montgomery(A, R(1))
			catch LoadError
				continue
			end
		end
		throw(ArgumentError("Could not find Montgomery model for E"))
	end
end
