######################################################################
# structures.jl: types and struct for elliptic curves.
######################################################################

export EllipticCurve, AbstractWeierstrass, ProjectivePoint

"""
Abstract type for elliptic curves.
"""
abstract type EllipticCurve{T<:Nemo.RingElem} end

"""
Abstract type for elliptic curves in general Weierstrass form.
"""
abstract type AbstractWeierstrass{T<:Nemo.RingElem} <: EllipticCurve{T} end

"""
Abstract type for projective points.
"""
abstract type ProjectivePoint{T<:Nemo.RingElem} end
