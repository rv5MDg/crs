######################################################################
# keys: Generation and functions related to keys
######################################################################

export keyGeneration, largestKey

"""
Key generation from the global lPrimes structure or specific bounds
"""
function keyGeneration(M_bounds = Dict())
	key = Dict()
	if length(M_bounds) == 0
		for l in keys(lPrimes)
			key[l] = rand(lPrimes[l].lBound:lPrimes[l].hBound)
		end
	else
		for l in keys(M_bounds)
			key[l] = rand(M_bounds[l]["lBound"]:M_bounds[l]["hBound"])
		end
	end
	return key
end

"""
The largest key possible.
"""
function largestKey(M_bounds = Dict())
	key = Dict()
	if length(M_bounds) == 0
		for l in keys(lPrimes)
			key[l] = max(abs(lPrimes[l].lBound), lPrimes[l].hBound)
		end
	else
		for l in keys(M_bounds)
			key[l] = max(abs(M_bounds[l]["lBound"]), M_bounds[l]["hBound"])
		end
	end
	return key
end
