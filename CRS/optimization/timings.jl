######################################################################
# timings.jl: Get elapsed time for each l-prime
######################################################################

include("../src/CRS.jl")
using JSON

using BenchmarkTools

"""
Elapsed time for each l-primes in the base lPrimes.
"""
function get_timings(k = 10)
	timings = Dict()
	for (l, lPrime) in CRS.lPrimes
		println("Checking prime l = ", l)

		## compilation warmup forward
		key = Dict(l => 1)
		CRS.walk(CRS.base_E, key)

		## actual timing forward
		key = Dict(l => k)
		r = @elapsed CRS.walk(CRS.base_E, key)
		timings[l] = Dict("forward" => r / k)
		println("	Mean forward: ", r/k)

		if lPrime.backward
			## compilation warmup backward
			key = Dict(l => -1)
			CRS.walk(CRS.base_E, key)

			## actual timing backward
			key = Dict(l => -k)
			r = @elapsed CRS.walk(CRS.base_E, key)
			timings[l]["backward"] = r / k
			println("	Mean backward: ", r/k)
		else
			timings[l]["backward"] = -1
		end
	end
	return timings
end

"""
Write timing results to a json file.
"""
function export_timings(k = 10)
	data = get_timings(k)
	open("../optimization/files/timings.json","w") do f
	    JSON.print(f, data)
	end
end
export_timings()

"""
Test worst key timing.
"""
function test_bounds()
	key = Dict()
	for (l, lPrime) in lPrimes
		key[l] = lPrime.highBound
	end

	Eprime_ = @time walk(E, key)
end

#test_bounds()
