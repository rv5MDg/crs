import numpy as np
import itertools
import json
import math
import collections

from scipy.optimize import minimize

from gekko import GEKKO

PATH_IN = "files/timings.json"
PATH_OUT = "files/optimized.json"

"""
    Load timing dictionary
"""
def load_data(max_l = 10000):
    with open(PATH_IN, "r") as f:
        data = json.load(f)
        converted = {int(key):value for key, value in data.items()}
        L, forward, backward = [], [], []
        for l, value in sorted(converted.items()):
            if  (l >= max_l): pass
            else:
                forward.append(value["forward"])
                if value["backward"] != -1: backward.append(value["backward"])
                L.append(l)
        return L, forward, backward, converted

"""
    Function to minimize: worst time case for generating the key
    This one is for python
"""
def to_optimize_(x):
    global L, FORWARD, BACKWARD, L_DICT
    res = 0
    for i in range(len(FORWARD)):
        l = L[i]
        f, b = L_DICT[l]["forward"], L_DICT[l]["backward"]
        if b == -1:
            res += x[i] * FORWARD[i]
        else:
            #res += x[i] * FORWARD[i]
            res += max( x[i] * FORWARD[i], x[i + len(FORWARD)] * BACKWARD[i])
            #res += ( x[i] * FORWARD[i] + x[i + len(FORWARD)] * BACKWARD[i])/2
    return res

"""
    Function to minimize: worst time case for generating the key
"""
def to_optimize(x, m):
    global L, FORWARD, BACKWARD, L_DICT
    res = 0
    for i in range(len(FORWARD)):
        l = L[i]
        f, b = L_DICT[l]["forward"], L_DICT[l]["backward"]
        if b == -1:
            res += x[i] * FORWARD[i]
        else:
            #res += x[i] * FORWARD[i]
            res += m.max3( x[i] * FORWARD[i], x[i + len(FORWARD)] * BACKWARD[i])
            #res += ( x[i] * FORWARD[i] + x[i + len(FORWARD)] * BACKWARD[i])/2
    return res

"""
    Function to compute the key-space size from GEKKO
"""
def keySpace_size(x, m):
    res = 0
    for i in range(len(x)):
        #res += m.log(2*x[i]+1) / m.log(2)
        res += m.log(x[i]) / m.log(2)
    return res

"""
    Function to compute the key-space size from python
"""
def keySpace_size_(x):
    res = 0
    for i in range(len(x)):
        #res += math.log(2*x[i]+1) / math.log(2)
        res += math.log(x[i]) / math.log(2)
    return res

"""
    GEKKO optimizer
"""
def optimize():
    global timings

    m = GEKKO(remote=False)
    x = m.Array(m.Var,len(TIMINGS),lb=1,ub=1000,integer=False)

    for i in range(len(TIMINGS)):
        f = TIMINGS[i]
        if 0.5 <= f <= 5:
            x[i].value = 5
            x[i].lower = 1
            x[i].upper = 100
        elif 0.05 <= f and f < 0.5:
            x[i].value = 10
            x[i].lower = 1
            x[i].upper = 1000
        elif 0.005 <= f and f < 0.05:
            x[i].value = 100
            x[i].lower = 1
            x[i].upper = 10000
        elif 0.001 <= f and f < 0.005:
            x[i].value = 1000
            x[i].lower = 4000
            x[i].upper = 15000

    print("Initial parameters:")
    y = [y.value.value for y in x]
    print(y)
    print("Initial time:")
    print(to_optimize_(y))
    print("Initial keySpace:")
    print(keySpace_size_(y))

    m.Equation(keySpace_size(x, m) >= 256)
    m.options.SOLVER=1
#    m.options.IMODE = 3
    m.options.COLDSTART=1
    #m.solver_options = ['minlp_maximum_iterations 10000']
    m.Minimize(to_optimize(x, m))
    m.solve(disp=True)
    return x

def save_result(res):
    y = [round(u[0]) for u in res]

    d = dict()
    j = 0
    for i in range(len(FORWARD)):
        l = L[i]
        d[l] = {"forward": y[i]}
        if L_DICT[l]["backward"] != -1:
            d[l]["backward"] = y[len(FORWARD):][j]
            j += 1
        else:
            d[l]["backward"] = 0

    with open(PATH_OUT, "w") as f:
        json.dump(d, f)

#Initialize global variables
max_l = 2000
L, FORWARD, BACKWARD, L_DICT = load_data(max_l)
TIMINGS = FORWARD + BACKWARD
len_FORWARD = len(FORWARD)

#Optimize bounds
res = optimize()

#Export optimized bounds
save_result(res)
